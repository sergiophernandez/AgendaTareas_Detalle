package com.sergio.jdbc.tareas;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Scanner;

import com.sergio.jdbc.excepciones.AgendaVaciaExcepcion;
import com.sergio.jdbc.excepciones.NoExisteEsaTareaExcepcion;

public class DBControlador {

	private Connection conn;
	private ArrayList<Tareas> agenda;
	private Scanner sc = new Scanner(System.in);;

	public DBControlador() {
		super();
		this.agenda = new ArrayList<Tareas>();
		try {
			this.conn = DriverManager.getConnection("jdbc:mysql://localhost:3307/tareas", "root", "usbw");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void anadirTarea(Tareas t) {
		PreparedStatement nps;
		try {
			nps = conn.prepareStatement("INSERT INTO tareas VALUES(?,?)");
			nps.setInt(1, 0);
			nps.setString(2, t.getNombre());
			nps.executeUpdate();
			agenda.add(t);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}

	public void anadirDetalle(Detalle d) {
		PreparedStatement ps;
		try {
			ps = conn.prepareStatement("INSERT INTO detalle VALUES(?,?,?,?)");
			ps.setInt(1, 0);
			ps.setInt(2, d.getId_tarea());
			ps.setString(3, d.getNombre());
			ps.setBoolean(4, d.isRealizado());
			ps.executeUpdate();

			//hay un problema, si el indice es 1 la lias.
			Tareas t = agenda.get(d.getId_tarea()-2);
			agenda.remove(d.getId_tarea()-2);
			t.anadirDetalle(d);
			agenda.add(d.getId_tarea()-2, t);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	public void realizarTarea(int id_t, int id_d) {
		Statement st;
		boolean realz = false;
		try {
			st = conn.createStatement();
			ResultSet rs = st
					.executeQuery("SELECT realizado FROM detalle WHERE id='" + id_d + "' AND id_tarea='" + id_t + "'");
			while (rs.next()) {
				realz = rs.getBoolean(1);
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		if (!realz) {
			PreparedStatement stmt;
			try {
				stmt = conn.prepareStatement("UPDATE detalle SET realizado=? WHERE id='" + id_d + "'");
				stmt.setBoolean(1, true);
				stmt.executeUpdate();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void verAgenda() {
		try {
			recuperarDatos();
		} catch (AgendaVaciaExcepcion e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		for (Tareas t : agenda) {
			System.out.println("ID: " + t.getId() + " DESC: " + t.getNombre());
			for (Detalle d : t.getDetalles()) {
				System.out.println("=== ID: " + d.getId() + " ID_TAREA: " + d.getId_tarea() + " NOMBRE: "
						+ d.getNombre() + " REALIZADO?: " + d.isRealizado());
			}
			System.out.println("");
		}
	}

	public void buscarActividad() throws NoExisteEsaTareaExcepcion {
		boolean found = false;
		System.out.print("Introduce NOMBRE tarea: ");
		String nombre_t = sc.nextLine(); 
		try {
			Statement st2 = conn.createStatement();
			ResultSet rs = st2.executeQuery("SELECT * FROM tareas");
			while (rs.next()) {
				if (rs.getString(2).equals(nombre_t)) {
					System.out.println("ID: " + rs.getInt(1) + " DESC: " + rs.getString(2));
					found = true;
					break;
				}
			}
			try {
				Statement st1 = conn.createStatement();
				ResultSet res = st1.executeQuery("SELECT * FROM detalle");
				while (res.next()) {
					if (res.getString(3).equals(nombre_t)) {
						System.out.println("=== ID: " + res.getInt(1) + " ID_TAREA: " + res.getInt(2) + " NOMBRE: "
								+ res.getString(3) + " REALIZADO?: " + res.getBoolean(4));
						found = true;
						break;
					}
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
		
		if(!found) {
			throw new NoExisteEsaTareaExcepcion("NO EXISTE LA ACTIVIDAD: " + nombre_t);
		}
	}

	private void recuperarDatos() throws AgendaVaciaExcepcion {
		if (!agenda.isEmpty()) {
			agenda.clear();
		}

		try {
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery("SELECT * FROM tareas");
			while (rs.next()) {
				Tareas t = new Tareas(rs.getInt(1), rs.getString(2));
				try {
					Statement st1 = conn.createStatement();
					ResultSet res = st1.executeQuery("SELECT * FROM detalle WHERE id_tarea='" + rs.getInt(1) + "'");
					while (res.next()) {
						Detalle d = new Detalle(res.getInt(1), res.getInt(2), res.getString(3), res.getBoolean(4));
						t.anadirDetalle(d);
					}
					agenda.add(t);

				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return;
				}
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}

		if (agenda.isEmpty()) {
			throw new AgendaVaciaExcepcion("LA BD ESTA VACIA, A�ADE UNA TAREA ANTES");
		}
	}

}
