package com.sergio.jdbc.tareas;

public class Detalle {

	private int id, id_tarea;
	private String nombre;
	private boolean realizado;
	
	public Detalle(int id, int id_tarea, String nombre, boolean realizado) {
		super();
		this.id = id;
		this.id_tarea = id_tarea;
		this.nombre = nombre;
		this.realizado = realizado;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getId_tarea() {
		return id_tarea;
	}

	public void setId_tarea(int id_tarea) {
		this.id_tarea = id_tarea;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public boolean isRealizado() {
		return realizado;
	}

	public void setRealizado(boolean realizado) {
		this.realizado = realizado;
	}
	
}
