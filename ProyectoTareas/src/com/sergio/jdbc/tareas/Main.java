package com.sergio.jdbc.tareas;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Scanner;

import com.sergio.jdbc.excepciones.NoExisteEsaTareaExcepcion;

public class Main {
	private static Scanner sc;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		boolean fin = false;
		DBControlador dbc = new DBControlador();
		sc = new Scanner(System.in);
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException ex) {
			System.out.println("No existe el Driver mysql");
		}

		do {
			System.out.println("");
			System.out.println("==================");
			System.out.println("===== Agenda =====");
			System.out.println("==================");
			System.out.println("");
			System.out.println("1 A�adir tarea");
			System.out.println("2 A�adir detalle tarea");
			System.out.println("3 Realizar tarea");
			System.out.println("4 Ver Agenda");
			System.out.println("5 Buscar actividad");
			System.out.println("6 Salir");
			System.out.println("");
			System.out.print("Introduzca una opcion: ");
			int opcion = Integer.valueOf(sc.nextLine());

			switch (opcion) {
			case 1:
				System.out.print("Introduce NOMBRE tarea: ");
				String nombre_t = sc.nextLine();
				Tareas t = new Tareas(0, nombre_t);
				dbc.anadirTarea(t);
				break;
			case 2:
				System.out.print("Introduce ID_TAREA: ");
				int id_t = Integer.valueOf(sc.nextLine());
				System.out.print("Introduce NOMBRE tarea: ");
				String nombre_t1 = sc.nextLine();
				System.out.print("Introduce REALIZADO?: ");
				boolean bool_t = Boolean.valueOf(sc.nextLine());
				Detalle d = new Detalle(0, id_t, nombre_t1, bool_t);
				dbc.anadirDetalle(d);
				break;
			case 3:
				System.out.print("Introduce ID de la tarea: ");
				int id_t1 = Integer.valueOf(sc.nextLine());
				System.out.print("Introduce ID del detalle: ");
				int id_d = Integer.valueOf(sc.nextLine());
				dbc.realizarTarea(id_t1, id_d);
				break;
			case 4:
				dbc.verAgenda();
				break;
			case 5:
				try {
					dbc.buscarActividad();
				} catch (NoExisteEsaTareaExcepcion e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				break;
			case 6:
				fin = true;
				break;

			}
		} while (!fin);
	}

}
