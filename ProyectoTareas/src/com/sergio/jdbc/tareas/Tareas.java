package com.sergio.jdbc.tareas;

import java.util.ArrayList;

public class Tareas {

	private ArrayList<Detalle> detalles;
	private int id;
	private String nombre;

	public Tareas() {
		super();
		this.detalles = new ArrayList<Detalle>();
	}

	public Tareas(int id, String nombre) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.detalles = new ArrayList<Detalle>();
	}

	public ArrayList<Detalle> getDetalles() {
		return detalles;
	}

	public void setDetalles(ArrayList<Detalle> detalles) {
		this.detalles = detalles;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void mostrarTareas() {
		
	}
	
	public void anadirDetalle(Detalle d) {
		this.detalles.add(d);
	}
	
}
